import axios from 'axios';
//const ENDPOINT3 = 'http://18.216.199.22:1337/api/v1/covid/deleteall'
const ENDPOINT = 'https://covid19.mathdro.id/api/countries/'
const ENDPOINT2 = 'http://18.216.199.22:1337/api/v1/covid/create'
import { addCountries } from "../../firebase/client";

export default async function CountriesLoad() {
  try {
    const resp = await axios.get(`${ENDPOINT}`);
    let data = resp.data;
    //console.log('countriess', data)
    const creados = data.countries.map(countrie => { 
      //console.log('countrie', countrie)
      let name = countrie.name;
      let iso2 = countrie.iso2;
      let iso3 = countrie.iso3;//recorriendo array de  paises
      addCountries2({name, iso2, iso3})
      async function addCountries2({ name, iso2, iso3 }) {
        console.log('registro:', name, iso2, iso3)
        try {
          const result = await addCountries({ name, iso2, iso3 })
          //return result;
        } catch (error) {
          return error;
        }
      }
      //console.log('entroooo ',name,iso2,iso3)
    });
    //console.log('Creados', creados)

    
    return 'ok'
  } catch (error) {
    return error;
  }
};