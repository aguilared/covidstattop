import axios from "axios";
const apiUrl = "http://18.216.199.22:1337/api/v1/countries";
export default async function getCountries() {
  try {
    const resp = await axios.get(apiUrl);
    let countries = resp;
    countries = countries.data.map((countrie) => {
      return { value: countrie.iso3, label: countrie.name };
    });
    return { countries };
  } catch (error) {
    return error;
  }
}
