import axios from "axios";
//const ENDPOINT3 = 'http://18.216.199.22:1337/api/v1/covid/deleteall'
const ENDPOINT = "https://covid19.mathdro.id/api/countries/";
import { addCovidDia, updateCovidDia } from "../../firebase/client";

export default async function CoviesLoad() {
  try {
    const resp = await axios.get(`${ENDPOINT}`);
    let data = resp.data;
    console.log("countries", data);
    const creados = data.countries.map((countrie) => {
      let iso3 = countrie.iso3;
      let name = countrie.name;
      let contador = 0;
      console.log("countrie",iso3, " count", contador + 1);

      const getCovidAsync = async ({ iso3, name }) => {
        const response = await axios.get(`${ENDPOINT}${iso3}`);
        let data = response.data;
        let countrie = iso3;
        let countriename = name;
        let deaths = data.deaths.value;
        let confirmed = data.confirmed.value;
        let recovered = data.recovered.value;

        const addCovidDia2 = async ({
          countrie,
          countriename,
          deaths,
          confirmed,
          recovered,
        }) => {
          console.log(
            "Guardando:",
            contador,
            countrie,
            countriename,
            deaths,
            confirmed,
            recovered
          );
          try {
            //const result = await addCovidDia({
            const result = await updateCovidDia({
              countrie,
              countriename,
              deaths,
              confirmed,
              recovered,
            });
            //console.log ('result despues d guardar', result) //return result;
          } catch (error) {
            return error;
          }
        };
        addCovidDia2({ countrie, countriename, deaths, confirmed, recovered });
      };
      getCovidAsync({ iso3, name }); //obteniniendo data de la api de covids
      // fin funcion
    });

    //console.log("Creados1", creados);
    return "ok";
  } catch (error) {
    return error;
  }
}
