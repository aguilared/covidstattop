import axios from 'axios';
const ENDPOINT3 = 'http://18.216.199.22:1337/api/v1/covid/deleteall'
const ENDPOINT = 'https://covid19.mathdro.id/api/countries/'
const ENDPOINT2 = 'http://18.216.199.22:1337/api/v1/covid/create'

export default async function CountriesLoad({ pais }) {
  try {

    const respDel = await axios.delete(`${ENDPOINT3}`);  
    //console.log('respdel',respDel)//Borrando datos de la tabla
    const resp = await axios.get(`${ENDPOINT}`);
    let datas = resp.data;
    const creados = datas.countries.map(data => {
      const countrie = data.iso3;
      getUserAsync(countrie)
      async function getUserAsync(countrie) {
        const response = await axios.get(`${ENDPOINT}${countrie}`);
        let data = response.data;
        let params = {
          countrie: countrie,
          confirmed: data.confirmed.value,
          deaths: data.deaths.value,
          recovered: data.recovered.value,
        }
        getUserAsync2(params) //crea un registro a la vez
        async function getUserAsync2(params) {
          //console.log('paramsss', params)
          try {
            const result = await axios.post(ENDPOINT2, params);
            //return result;
          } catch (error) {
            return error;
          }
        }
      }
    })
    console.log('creados',creados)
    return 'ok'
  } catch (error) {
    return error;
  }
};