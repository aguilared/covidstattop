import { useState, useEffect } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";


import Button from "../../components/Button";

import Link from "next/link";
import { useRouter } from "next/router";

import * as firebase from "firebase";
import { addCountries } from "../../firebase/client";
import { fetchCountries } from "../../firebase/client";

import getCountries from "../../components/Load/getCountries";
import CountriesLoad from "../../components/Load/CountriesLoad";
const db = firebase.firestore();
const COMPOSE_STATES = {
  USER_NOT_KNOWN: 0,
  LOADING: 1,
  SUCCESS: 2,
  ERROR: -1,
};

console.log("Countries", getCountries());
console.log("CountriesFetch", fetchCountries());


const links = [
  { href: "https://zeit.co/now", label: "ZEIT" },
  { href: "https://github.com/zeit/next.js", label: "GitHub" },
].map((link) => {
  link.key = `nav-link-${link.href}-${link.label}`;
  return link;
});

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);


const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);


const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function ComposeTweet() {
  const [rows, setRows] = useState([]);

  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [countries, setCountries] = useState([]);
    
  useEffect(
    function () {
      setLoading(true);
      fetchCountries().then((countries) => {
        setLoading(false);
        setCountries(countries);
      });
    },
    [setCountries]
  );


  console.log("Countriesluego", countries);
  const [name, setName] = useState("");
  const [iso2, setIso2] = useState("");
  const [iso3, setIso3] = useState("");

  const [status, setStatus] = useState(COMPOSE_STATES.USER_NOT_KNOWN);
  const router = useRouter();

  const handleChange = (event) => {
    const { value } = event.target;
    setName(value);
    setIso2(value);
    setIso3(value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setStatus(COMPOSE_STATES.LOADING);
    addCountries({
      name: name,
      iso2: iso2,
      iso3: iso3,
    })
      .then(() => {
        router.push("/");
      })
      .catch((err) => {
        console.error(err);
        setStatus(COMPOSE_STATES.ERROR);
      });
  };

  const isButtonDisabled = !name.length || status === COMPOSE_STATES.LOADING;

  return (
    <>
      <nav>
        <ul>
          <li>
            <Link href="/">
              <a>Home</a>
            </Link>
          </li>
        </ul>

        <style jsx>{`
          div {
            padding: 15px;
          }

          textarea {
            border: 0;
            font-size: 21px;
            min-height: 200px;
            padding: 15px;
            outline: 0;
            resize: none;
            width: 100%;
          }
          input {
            border: 0;
            font-size: 21px;
            min-height: 200px;
            padding: 15px;
            outline: 0;
            resize: none;
            width: 100%;
          }
          :global(body) {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
              Helvetica, sans-serif;
          }
          nav {
            text-align: center;
          }
          ul {
            display: flex;
            justify-content: space-between;
          }
          nav > ul {
            padding: 4px 16px;
          }
          li {
            display: flex;
            padding: 6px 8px;
          }
          a {
            color: #067df7;
            text-decoration: none;
            font-size: 13px;
          }
        `}</style>
      </nav>

      <div className="App">
        <div className="container mx-auto">
          <h3 className="text-center  text-3xl mt-20 text-base leading-8 text-black font-bold tracking-wide uppercase">
            Venezuela Segura. Covid-19 Top mundial - Loaddd
          </h3>
          <div className="md:flex">
            {countries.map((row, index) => (
              <form onSubmit={handleSubmit}>
                <input
                  onChange={(e) => setName(e.target.value)}
                  placeholder="Countrie"
                  value={row.name}
                ></input>
                <input
                  onChange={(e) => setIso2(e.target.value)}
                  placeholder="Iso2"
                  value={row.iso2}
                ></input>
                <input
                  onChange={(e) => setIso3(e.target.value)}
                  placeholder="Iso3"
                  value={row.iso3}
                ></input>
                <div>
                  <Button disabled={isButtonDisabled}>Guardar</Button>
                </div>
              </form>
            ))}
          </div>

          {countries.map((row, index) => (
            <StyledTableRow key={index}>
              <StyledTableCell component="th" scope="row">
                {index + 1}
              </StyledTableCell>
              <StyledTableCell component="th" scope="row">
                {row.name}
              </StyledTableCell>
              <StyledTableCell align="right">{row.iso2}</StyledTableCell>
              <StyledTableCell align="right">{row.iso3}</StyledTableCell>
            </StyledTableRow>
          ))}
        </div>
      </div>
    </>
  );
}
