import { useState, useEffect } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";


import Button from "../../components/Button";

import Link from "next/link";
import { useRouter } from "next/router";

import * as firebase from "firebase";
import { addCountries } from "../../firebase/client";
import { fetchCountries } from "../../firebase/client";

import CountriesLoad from "../../components/Load/CountriesLoad";
const db = firebase.firestore();
const COMPOSE_STATES = {
  USER_NOT_KNOWN: 0,
  LOADING: 1,
  SUCCESS: 2,
  ERROR: -1,
};

console.log("CountriesFetch", fetchCountries());


const links = [
  { href: "https://zeit.co/now", label: "ZEIT" },
  { href: "https://github.com/zeit/next.js", label: "GitHub" },
].map((link) => {
  link.key = `nav-link-${link.href}-${link.label}`;
  return link;
});

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);


const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);


const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function ComposeTweet() {

  const classes = useStyles();
 
  

  const [status, setStatus] = useState(COMPOSE_STATES.USER_NOT_KNOWN);
  const router = useRouter();


  const handleSubmit = (event) => {
    event.preventDefault();
    setStatus(COMPOSE_STATES.LOADING);
    CountriesLoad()
      .then((resp) => {
        if (resp == 'ok')
        console.log('Cargo datos', )
        router.push("/countries/");
      })
      .catch((err) => {
        console.error(err);
        setStatus(COMPOSE_STATES.ERROR);
      });
  };
  

  const isButtonDisabled = !COMPOSE_STATES.LOADING.length || status === COMPOSE_STATES.LOADING;

  return (
    <>
      <nav>
        <ul>
          <li>
            <Link href="/">
              <a>Home</a>
            </Link>
          </li>
        </ul>

        <style jsx>{`
          div {
            padding: 15px;
          }

          textarea {
            border: 0;
            font-size: 21px;
            min-height: 200px;
            padding: 15px;
            outline: 0;
            resize: none;
            width: 100%;
          }
          input {
            border: 0;
            font-size: 21px;
            min-height: 200px;
            padding: 15px;
            outline: 0;
            resize: none;
            width: 100%;
          }
          :global(body) {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
              Helvetica, sans-serif;
          }
          nav {
            text-align: center;
          }
          ul {
            display: flex;
            justify-content: space-between;
          }
          nav > ul {
            padding: 4px 16px;
          }
          li {
            display: flex;
            padding: 6px 8px;
          }
          a {
            color: #067df7;
            text-decoration: none;
            font-size: 13px;
          }
        `}</style>
      </nav>

      <div className="App">
        <div className="container mx-auto">
          <h3 className="text-center mt-20 text-base leading-8 text-black font-bold tracking-wide uppercase">
            Venezuela Segura. Covid-19 Top mundial - Load Countries
          </h3>
          <div className="md:flex">
              <form onSubmit={handleSubmit}>
                
                <div>
                  <Button >Load Countries</Button>
                </div>
              </form>
          </div>

          
        </div>
      </div>
    </>
  );
}
